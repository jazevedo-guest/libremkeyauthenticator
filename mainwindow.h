/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <memory>

#include <zbar/QZBar.h>

#include <QErrorMessage>
#include <QMainWindow>
#include <QTimer>

#include "aboutlibremkeydialog.h"
#include "keytotpmodel.h"
#include "totpuriparser.h"

class AuthenticateDialog;
class Authentication;
class LibremKeyProvider;

namespace Ui
{
    class MainWindow;
}

/**
 * @brief Class representing the main window of the application.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Constructs main window.
     * but instead is passed to the model.
     * @param model Model for representing TOTP slots from the USB key in the view.
     * @param parent Parent widget (Qt-specific parameter).
     */
    explicit MainWindow(std::shared_ptr<KeyTOTPModel> model,
                        QWidget *parent = nullptr);

    ~MainWindow();

public slots:
    /**
     * @brief Handles event of clicking the "+" tool button
     * for addition of a new secret.
     */
    void onAddSecretClicked();

private slots:
    void onQRCodeDecoded(const QString &);
    void onDecoded();
    void onSlotSelected();
    void onUserAuthenticationCanceled();
    void onUserAuthenticationFailed();
    void onQrCodeTimerTimeout();
    void onAboutLibremkeyAuthenticator();
    void onCopyClicked() const;

private:
    Ui::MainWindow *ui;
    std::shared_ptr<KeyTOTPModel> model;
    zbar::QZBar scanner;
    QErrorMessage errorMessage;
    QTimer qrCodeTimer;
    AboutLibremKeyDialog aboutLibremKeyDialog;
};

#endif // MAINWINDOW_H
