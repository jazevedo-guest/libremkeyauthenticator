#include "authenticationexception.h"

AuthenticationException::AuthenticationException(const std::string &message)
    : std::runtime_error(message)
{

}
