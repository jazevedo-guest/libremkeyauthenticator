# List of 3rd party software used in this project
Below is a list of 3rd party software together with their corresponding licenses.

## hidapi
* Homepage: [GitHub](https://github.com/libusb/hidapi)
* License: [HIDAPI](licenses/HIDAPI.txt)

## Qt 5
* Homepage: [doc.qt.io](https://doc.qt.io)
* License: [LGPL-3](licenses/LGPL-3)

## libnitrokey
* Homepage: [GitHub](https://github.com/Nitrokey/libnitrokey)
* License: [LGPL-3](licenses/LGPL-3)

## libzbar
* Homepage: [SourceForge](http://zbar.sourceforge.net)
* License: [LGPL-2.1](licenses/LGPL-2.1)

## GoogleTest
* Homepage: [GitHub](https://github.com/google/googletest)
* License: [BSD 3 clauses-like](licenses/googletest_license.txt)

