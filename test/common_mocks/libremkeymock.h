#ifndef LIBREMKEYMOCK_H
#define LIBREMKEYMOCK_H

#include "gmock/gmock.h"

#include "libremkeybase.h"

class LibremKeyMock : public LibremKeyBase {
public:
    MOCK_METHOD(bool, connect, (), (override));
    MOCK_METHOD(bool, isConnected, (), (const, override));
    MOCK_METHOD(bool, firstAuth, (const std::string &, const std::string &),
                (override));
    MOCK_METHOD(void, userAuth, (const std::string &, const std::string &),
                (override));
    MOCK_METHOD(void, writeTotpSlot, (std::uint8_t, const std::string &,
                                      const std::string &, std::uint16_t,
                                      const std::string &), (override));
    MOCK_METHOD(void, eraseTotpSlot, (std::uint8_t, const std::string &),
                (override));
    MOCK_METHOD(std::vector<TOTPSlot>, getSlots, (), (const, override));
    MOCK_METHOD(std::string, getSlotName, (std::uint8_t), (const, override));
    MOCK_METHOD(void, setTime, (std::uint64_t), (const, override));
    MOCK_METHOD(void, setTimeSoft, (std::uint64_t), (const, override));
    MOCK_METHOD(std::string, getTOTPCode, (const TOTPSlot &,
                                           const std::string &),
                (const, override));
};


#endif // LIBREMKEYMOCK_H
