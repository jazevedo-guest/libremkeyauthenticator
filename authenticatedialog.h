/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef AUTHENTICATEDIALOG_H
#define AUTHENTICATEDIALOG_H

#include <QDialog>

namespace Ui {
class AuthenticateDialog;
}

/**
 * @brief A simple modal dialog box for entering PIN (both, Admin and User).
 */
class AuthenticateDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Represents the type of requested PIN entry.
     */
    enum class Type
    {
        ADMIN,
        USER
    };

    /**
     * @brief Constructs an instance of the dialog box.
     * @param authType Based on the value of this parameter, dialog will ask either for
     * Admin or User PIN.
     * @param parent Standard Qt's parent object.
     */
    explicit AuthenticateDialog(Type authType, QWidget *parent = nullptr);

    /**
     * @brief Gracefully deletes an instance of the class.
     */
    ~AuthenticateDialog();

    /**
     * @brief Returns PIN entered by the user.
     * @return PIN.
     */
    virtual QString pin() const;

    /**
     * @brief Returns authentication type (ADMIN, or USER).
     * @return Type::ADMIN if the dialog is for admin authentication,
     * Type::USER otherwise.
     */
    virtual Type authenticationType() const { return authType; }

public slots:
    /**
     * @brief Cleans PIN (possibly entered previously by the user) and
     * calls patent's exec().
     * @return QDialog::Accepted or QDialog::Rejected.
     */
    int exec() override;

private slots:
    void onShowPinStateChanged(int);

private:
    Ui::AuthenticateDialog *ui;
    const Type authType;
};

#endif // AUTHENTICATEDIALOG_H
