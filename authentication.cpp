/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "authentication.h"
#include "unauthenticatedexception.h"

#include <algorithm>

#include <boost/log/trivial.hpp>

#include <QTimer>

Authentication::Authentication(std::shared_ptr<AuthenticateDialog> authenticateDialog,
                               std::shared_ptr<LibremKeyProvider> keyProvider,
                               unsigned int sessionTimeoutMSec)
    : authenticateDialog(authenticateDialog),
      tempPasswordGenerator(),
      tempPassword{0},
      keyProvider(keyProvider),
      sessionTimeoutMSec(sessionTimeoutMSec)
{
    if (AuthenticateDialog::Type::ADMIN == authenticateDialog->authenticationType()) {
        connect(keyProvider.get(), &LibremKeyProvider::adminAuthenticated,
                this, &Authentication::onAuthenticated);
        connect(keyProvider.get(), &LibremKeyProvider::adminAuthenticationFailure,
                this, &Authentication::onAuthenticationFailure);
    } else {
        connect(keyProvider.get(), &LibremKeyProvider::userAuthenticated,
                this, &Authentication::onAuthenticated);
        connect(keyProvider.get(), &LibremKeyProvider::userAuthenticationFailure,
                this, &Authentication::onAuthenticationFailure);
    }
}

void Authentication::onAuthenticated()
{
    QTimer::singleShot(sessionTimeoutMSec, this, &Authentication::scrubPassword);
    emit authenticated();
}

void Authentication::onAuthenticationFailure()
{
    scrubPassword();
    emit authenticationFailure();
}

bool Authentication::checkIfAuthenticated() const
{
    auto passwordScrubbed =
            std::all_of(++(tempPassword.rbegin()), tempPassword.rend(), [](const unsigned char ch)
    {
        return (0 == ch);
    });

    return !passwordScrubbed;
}

void Authentication::scrubPassword()
{
    Lock lock(tempPasswordMutex);
    std::for_each(tempPassword.begin(), tempPassword.end(), [](unsigned char &ch)
    {
        ch = 0;
    });
}

void Authentication::authenticate()
{
    scrubPassword();
    tempPassword = tempPasswordGenerator.generatePassword();
    if (QDialog::Accepted == authenticateDialog->exec()) {
        BOOST_LOG_TRIVIAL(info) << "User provided pin, so authenticating.";
        auto pin = authenticateDialog->pin();
        if (AuthenticateDialog::Type::ADMIN == authenticateDialog->authenticationType()) {
            BOOST_LOG_TRIVIAL(debug) << "About to perform Admin authentication.";
            keyProvider->adminAuth(pin, tempPassword);
        } else {
            BOOST_LOG_TRIVIAL(debug) << "About to perform User authentication.";
            keyProvider->userAuth(pin, tempPassword);
        }
    } else {
        BOOST_LOG_TRIVIAL(info) << "authenticate(). Authentication canceled by the user.";
        scrubPassword();
        emit authenticationCanceled();
    }
}

RandomPasswordGenerator::KeyPassword Authentication::password()
{
    Lock lock(tempPasswordMutex);
    if (!checkIfAuthenticated()) {
        throw UnauthenticatedException("Not authenticated");
    }
    return tempPassword;
}
