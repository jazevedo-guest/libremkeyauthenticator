/*
 * BSD 2-Clause License
 *
 * Copyright (c) 2020, Agnieszka Cicha-Cisek
 * Copyright (c) 2020, Patryk Cisek
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NEWTOTPSLOTDIALOG_H
#define NEWTOTPSLOTDIALOG_H

#include "totpuriparser.h"

#include <QDialog>

namespace Ui {
class NewTOTPSlotDialog;
}

/**
 * @brief A GUI class that represents a modal dialog that asks user for new
 * TOTP slot name.
 */
class NewTOTPSlotDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Constructs instance of the dialog.
     * @param parent A Qt parent of the dialog.
     */
    explicit NewTOTPSlotDialog(QWidget *parent = nullptr);

    /**
     * @brief Destructs instance.
     */
    ~NewTOTPSlotDialog();

    /**
     * @brief Presets the name and secret of the dialog box.
     * @param uriParser URI parser with values to be used in the dialog.
     */
    void presetNameAndSecret(const TotpUriParser &uriParser);

    /**
     * @brief Returns name, that user provided.
     * @return New TOTP slot's name.
     */
    virtual QString slotName() const;

private:
    Ui::NewTOTPSlotDialog *ui;
};

#endif // NEWTOTPSLOTDIALOG_H
